IT, ist das was für mich?
=========================

🚧 **Diese Seite ist noch im Entstehen** 👷‍♀️

Arbeitsmaterialien für den Kurs.


.. toctree::
   :maxdepth: 2

   course/index.rst

   lego/index.rst
   microbit/index

   learning.md
   orientation.md
   mode.md
   contact.md

..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
