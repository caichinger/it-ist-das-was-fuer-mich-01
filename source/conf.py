# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = "IT, ist das was für mich?"
copyright = "2021, everyone codes"
author = "Claus Aichinger, Olga Drewitz"

# https://sphinx-book-theme.readthedocs.io/en/latest/customize/sidebar-primary.html
html_title = "Arbeitsmaterialien für Berufsorientierung IT"
html_logo = "_static/ec_logo.png"
html_favicon = "_static/ec_favicon.png"

# -- Project data ------------------------------------------------------------

# Substitutions replace
# With regards to links, see https://myst-parser.readthedocs.io/en/latest/syntax/optional.html?highlight=admonition#substitutions-and-urls
myst_substitutions = {
    #
    "doc_Berufe_und_Kurse": "https://docs.google.com/document/d/1tm0BvkZtS-oFaGNKqKZmDmYGNcyq4N16Fcz4SWbXYk8/edit?usp=sharing",
}



# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_parser",
    "sphinx_togglebutton",  # https://sphinx-togglebutton.readthedocs.io/
    "sphinxcontrib.mermaid",  # https://github.com/mgaitan/sphinxcontrib-mermaid
    "sphinx_exercise",  # https://github.com/executablebooks/sphinx-exercise
    "sphinx_tabs.tabs",  # https://github.com/executablebooks/sphinx-tabs
    "sphinx_copybutton",  # https://github.com/executablebooks/sphinx-copybutton
    "sphinxemoji.sphinxemoji",  # https://github.com/sphinx-contrib/emojicodes
]

# MyST extensions
myst_enable_extensions = [
    "html_image",  # https://myst-parser.readthedocs.io/en/latest/syntax/optional.html#syntax-images
    "linkify",  # to render raw links as html links
    "substitution",  # https://myst-parser.readthedocs.io/en/latest/syntax/optional.html?highlight=admonition#substitutions-with-jinja2
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    "_build",
    "**/todo.md",
    "**/day_xx_template/*",
    "**/internal.md",
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_book_theme"  # https://github.com/executablebooks/sphinx-book-theme

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


# -- Linkcheck ---------------------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html?highlight=linkcheck#options-for-the-linkcheck-builder
linkcheck_ignore = [
    r'https://microbit.org/join',
    r'https://www.mipumi.com/',
    r'https://www.waveshare.com/piano-for-micro-bit.htm',
    r'https://www.waveshare.com/media/catalog/product/cache/1/thumbnail/122x122/9df78eab33525d08d6e5fb8d27136e95/p/i/piano-for-micro-bit-4.jpg',
]
