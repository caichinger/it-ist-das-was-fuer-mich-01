# Tag 9: BBC micro:bit Intro I

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`


Wir machen heute folgende Übungen
1. {ref}`exercise-micro-bit-flashing-heart`
1. {ref}`exercise-micro-bit-chrome-webusb`
1. {ref}`exercise-micro-bit-name-tag`
1. {ref}`exercise-micro-bit-dice`
