---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (claus.aichinger@gmail.com)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 9

Wir probieren was neues:

Programmieren mit dem BBC micro:bit!

---

## Check In

### Wie geht es uns?
### Wie geht es dir?

*2 von 5 Wochen sind schon vergangen.*

---

## Administratives

Olga:
> **Arbeitsunfähigkeitsmeldungen bis spätestens morgen bringen.**
> Morgen geht Eintrag an das AMS.
> Ohne Arbeitsunfähigkeitsmeldung ist es unentschuldigtes Fehlen.

---

## Intro 🌅

*Big Picture*

---

## Was passiert heute? 🗒️

- Standortbestimmung
- Wiederholung
- Neue Hardware (micro:bit)
- Neue Möglichkeiten

---

## Was war letzte Woche? 🤔

*Wiederholung*

- Was habt ihr gelernt?
- Wie ist es euch ergangen?
- Welche Programmbefehle habt ihr kennen gelernt?

---

## ❗ Wichtige Fragen ❓

Bei allem was wir tun, sollten wir diese Fragen beantworten können:

- **Was?**
- **Warum?**
- **Wie?**

Wenn wir das nicht können, wissen wir nicht, was wir tun.

*Heute, diese Woche (und überhaupt) stellen wir uns diese Fragen.*

---

## Teamwork und Gruppen 🤝

Es ist wichtig, explizit auszusprechen was man will und tut.

Das braucht Übung.

*Heute (und je nachdem wie es läuft) werden wir die Gruppen jede Übung neu mischen.*

---

## micro:bit 🎈

Was haben wir vor?

- Den Computer entdecken
- Verstehen, wie man ihn programmiert
- Neue Programmiersprachen kennen lernen
- Überlegen, was man alles mit ihm machen kann

---

# Pause ☕

Gleich geht's weiter.

---

## Was haben wir heute gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Outro 🌆

*Tagesabschluss*

---

### Was war heute?

- Neuer Modus
- Neue Hardware
- Neue Software

---

### Was kommt morgen

- Fortsetzung von heute
- Mehr in Richtung Programmierung
- Mehr in Richtung Sensoren

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Ich freue mich auf morgen! 😃
