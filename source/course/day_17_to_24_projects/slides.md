---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (claus.aichinger@gmail.com)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 17 - 24

Projektarbeit

---

# Intro 🌅

## Was passiert heute? 🗒️

* Projektearbeit
* Einzelgespräche zu Berufen und Ausbildungen

---

## Grober Zeitplan 📆

| Tag | Thema |
|-----|-------|
| Fr, 10.12. | Vorbereitung der Projekte |
| Mo, 13.12. bis Mi, 15.12. | Umsetzung der Projekte |
| Do, 16.12. | Peer Learning |
| Fr, 17.12. | Abschlusspräsentationen (+ Frühstück 🥐☕) |

*Zusätzlich Einzelgespräche.*

---

# Projekte 🏗️

*Ideen und Vorschläge*

---

## Dokumentation 📸 📄

Inhalte und Ziele:
* Festhalten was in den Projekten passiert
* Fotos machen
* Projekttagebuch

Role: Fotografin und Documentarian

Person: Simi

---

## Betreuung 🤝

Inhalte und Ziele:
* Den Teilnehmerinnen mit Rat und Tat zur Seite stehen
* Zuhören und erklären lassen
* Ideen einbringen und Fragen stellen

Rolle: Coach

Person: Viyan

---

## Lego City Shaper Challenge 🏙️

Inhalte und Ziele:
* Space Challenge lösen

Rolle: Space Engineer

Personen: Sham, Lejla

---

## Micro Bit + Erweiterung/Programmiersprache? 🤖

Inhalte und Ziele:
* Größeres Projekt mit dem Micro Bit
* Varianten
  * Erweiterung (Game ZIP, Roboter Board, ...)
  * Programmiersprachen Python oder JavaScript

Rolle: Programmiererin

Personen: Manuela, Sara

---

## Micro Bit + E-Textile/Piano/...? 🎊

Inhalte und Ziele:
* Kreativprojekt mit dem Micro Bit
* Varianten
  * Piano
  * E-Textile (LEDs)
  * LED ZIP Halo (LEDs)

Rolle: Digital Artist

Personen: Sanaa, Nurten

---

# [Berufsorientierung](https://caichinger.gitlab.io/) IT

## Wie geht es nach dem Kurs weiter? 🧭

* Bitte weiterhin Berufe und Kurse in unserem Dokument sammeln.
* Wir führen diese Woche Gespräche mit
  * euch
  * dem AMS
  * und anderen Institutionen


* Wisst ihr, dass es Betreuungsangebote gibt?

---

# Outro 🌆

## Was war heute?

- Projektarbeit

---

## Was haben wir heute gelernt? 📝

Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Was kommt nächstes Mal?

- Weiter bei Projekten
- Weiter bei Berufen und Ausbildungen

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Wir freuen uns auf die nächste Einheit! 😃
