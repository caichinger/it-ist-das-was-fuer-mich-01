# Tag 17 - Tag 24: Projektarbeit

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`

Heute werden wir mit den Projekten loslegen.

Wir haben für jede von euch ein eigenes Dokument angelegt, in
dem ihr euer Projekt beschreiben und planen könnt.

## Lejla
* [Textdokument](https://docs.google.com/document/d/1kwbyBWP5jK3HuSxqzCL1sx5dYFoBY-Hv0-5GOerZSf8/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1n9PHItYNru8Z-7MF2R5jkXtAdvPQF81S-owZgM3Nx5k/edit?usp=sharing)

## Manuela
* [Textdokument](https://docs.google.com/document/d/1K4kDgH2PYnH32dDOEB1CkLhRb40lU_PlihJyALTsJO0/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1uI1j1u2ZNFDTSAzMoh5JGsmQ46dbe_xUEfhtltYmZzo/edit?usp=sharing)

## Nurten
* [Textdokument](https://docs.google.com/document/d/1sOhhC2WIIieNiGJDUivhJCZCdzMqG15-K2LvUytlOT0/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1PCQRA6CPBft0CZP4AycdAWwAYU-EP9thAkieeUrYwW8/edit?usp=sharing)

## Sanaa
* [Textdokument](https://docs.google.com/document/d/1ydUdWiukYb7DhbAJJSXmoYDul4FRntYP6NpfGhFaMf0/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1GU7HclNym8VeRkH6-IGaKLrDA98-J1Ef-Mm6r33oO2o/edit?usp=sharing)

## Sara
* [Textdokument](https://docs.google.com/document/d/1uMIH14A8rRHAjv-zIHKGXngfKRzXTHhSDX1Qxrgx064/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1clxz3VgNdB2qGh_bQ0yzYUFef2Se8fCOkkZyR5hF6-Y/edit?usp=sharing)

## Sham
* [Textdokument](https://docs.google.com/document/d/12-c9NbTDKa8iDTkACpeFr1K4WSafk7vizm3QFcq5eqU/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1ENXC6jKiKyug_G888yD2wo6tKs1KTeR2FlJd2VBFLUg/edit?usp=sharing)

## Simran
* [Textdokument](https://docs.google.com/document/d/1-6TC48NnYyRArzVruKyBT0MyHahj1VfM-DREXnU1Fe4/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1hOcsfvqzXsB5XvBwQnbKdEjqTbR10YT9vTbGZRVyoDg/edit?usp=sharing)

## Viyan
* [Textdokument](https://docs.google.com/document/d/1r5VsK9hfXFaGGs1XeoAycig3QZmaSK6PADEqI8K0xnc/edit?usp=sharing)
* [Präsentation](https://docs.google.com/presentation/d/1mNgQIhAxIknKR8LRGuFKTFJz9WPPgARN8ZABv-1pw7I/edit?usp=sharing)
