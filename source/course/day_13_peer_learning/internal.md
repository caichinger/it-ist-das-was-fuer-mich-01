# Documentation

*Copy-paste*

Die Unterlagen sind wie bisher auf der Website. Das wissen sie und wir arbeiten in der Klasse auch damit.
Falls es wider Erwarten Probleme gibt, das sind die Links:

    https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/
    https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/course/day_13_peer_learning/index.html

Es gibt drei Themenblöcke:

    Ihre Projekte. Haben wir zwar besprochen aber ich rechne damit das die beiden Gruppen die nähen werden, Schwierigkeiten mit der Verkabelung haben werden. Sie sollten alles was sie brauchen mitbringen aber falls notwendig, liegen im Moderationskoffer Nadeln (leitender Fade ist in den kleinen Säckchen am Tisch). Wichtig ist, dass auch wenn sie Schwierigkeiten haben, sie einzelne Teile der Arbeit so weit ihnen möglich ist fertig machen (besprochen und geübt).
    Micro Bit Features. In neu gemischten Gruppen.
    Berufsbilder und Kurse. Hier sollen sie ein Selbsteinschätzung machen.

Ich würde für den ersten Teil circa eine 1,5h geben. Manuela wird vielleicht schneller fertig, für sie gibt es noch eine Ergänzung bzw. kann sie auch die Chat/Radio-Übung probieren, kann mir vorstellen, dass ihr das taugt.Der dritte Teil ist wichtig, weil ich wissen möchte, was vom Mittwoch da ist und ob bzw. vorallem was sie für sich mitgenommen haben.Der zweite Teil ist Übergang/Puffer.@Sashka (team) je nachdem wie die Stimmung ist, tausche bitte 2 und 3. Das überlasse ich deiner Einschätzung.In Summe sind wir sicher eher anspruchsvoll unterwegs, der Umgang mit Schwierigkeiten ist Teil der Übung.
Wir setzen die Aufgaben am Montag fort. D.h. wenn sie Probleme haben, sollen sie versuchen versuchen soweit wie möglich zu kommen und das "Restproblem" zu konkretisieren und beschreiben.Wenn es was gibt, gerne melden. Bitte zur Sicherheit per Signal/Telefon (falls wichtig), sonst bekomme ich es vielleicht nicht mit. ;)
