# Tag 13: Peer Learning

Gratulation zur dritten Kurswoche! 🎊

Heute steht wieder Peer Learning am Programm.

```{include} ../_peer_learning.md
```

Für heute sind drei Themenblöcke vorgesehen:
1. Umsetzung eurer Projektideen
   - {ref}`exercise-project-step-counter`
   - {ref}`exercise-project-emoji-mask`
   - {ref}`exercise-project-wristband`
1. {ref}`Weitere Sensoren und Features vom Micro Bit erkunden <exercise-microbit-features-and-sensors>`
1. {ref}`Berufsbilder und Ausbildungen <exercise-job-self-assessment>`


```{exercise} Projekt: Schrittzähler
:label: exercise-project-step-counter

[Projektbeschreibung](https://docs.google.com/document/d/1w1-rwysRbUHyuK4NGRuFHlSWtvWRQ-K1eEUdKhCtvko/edit?usp=sharing)

Fülle die Projektbeschreibung aus.

Idee zur Ergänzung:
Du könntest dein Programm so erweitern, dass man zu Beginn
das Tagesziel angeben kann.
```

```{important}
Wie in der Klasse besprochen:
- Um eine LED anzusteuern, muss man den (-)-Anschluss mit GND und
  den (+) mit einem Pin verbinden. Das nennt man einen Schaltkreis.
- In einem Schaltkreis können auch mehrere LEDs gleichzeitig ein- und ausgeschalten
  werden. Dazu müssen alle (-)-Anschlüsse miteinander verbunden sein und getrennt davon alle
  (+)-Anschlüsse.
- Möchte man LEDs separat ansteuern, müssen diese an einem eigenen Schaltkreis hängen.
- Unabhängige Schaltkreise erreicht man, in dem die (+)-Leitung an jeweils verschiedenen Pins
  hängt (die (-)-Leitungen hängen alle an GND).
- Die Verbindung mit dem Micro Bit kann beispielsweise über die Klemmen erfolgen.
```

```{exercise} Projekt: Emoji Maske
:label: exercise-project-emoji-mask

[Projektbeschreibung](https://docs.google.com/document/d/1imdCila1CPV0_yUMmok2HHiSptd5AxXuV8i68zP26EA/edit?usp=sharing)

Fülle die Projektbeschreibung aus.
Beschreibe dort auch, wann/wie 💗 und ⭐ leuchten sollen.

Schreibe ein Programm, das das gewünschte Verhalten zeigt.
Zum einfacheren Ausprobieren zeige den Output deines
Programm über Herz und Stern Symbole an. Später änderst du
die Ausgabe und schaltest die Pins an und aus.

Beim Aufnähen der LEDs achte auf die richtige Polung.

Wenn du fertig bist, verbinde die 2 LED Schaltkreise mit dem Micro Bit
und ändere das Programm so ab, dass die Pins angesteuert werden.
```

```{exercise} Projekt: Armband
:label: exercise-project-wristband

[Projektbeschreibung](https://docs.google.com/document/d/1q0xNIHzu2hqLqwAlYlum2ulvNFJpqZ4dJ51KKA8AfZI/edit?usp=sharing)

Fülle die Projektbeschreibung aus.
Beschreibe dort auch, wann/wie welche LEDs leuchten sollen.

Schreibe ein Programm, das das gewünschte Verhalten zeigt.
Zum einfacheren Ausprobieren zeige den Output deines
Programm über das Display an (sie LED Blöcke in Make Code).
Später änderst du diese Ausgabe und schaltest die Pins an und aus.

Beim Aufnähen der LEDs achte auf die richtige Polung.

Wenn du fertig bist, verbinde die LED Schaltkreise mit dem Micro Bit
und ändere das Programm so ab, dass die Pins angesteuert werden.
```


```{exercise} Micro Bit Features und Sensoren
:label: exercise-microbit-features-and-sensors

Bildet Gruppen.
Am besten arbeitet ihr mit einer Person zusammen, mit der ihr
noch nicht zusammen gearbeitet habt.
Jede Gruppe übernimmt eine der folgenden Aufgaben und präsentiert die Lösung
anschließend allen.

Wenn ihr Schwierigkeiten habt, helft euch gegenseitig.
Die Übungen sind in aufsteigendem Schwierigkeitsgrad.


**Clap Lights (Klatschschalter)**

> Wie baut man einen Klatschschalter mit dem Micro Bit?

Der **V2 (und nur dieser!)** hat ein Mikrofon eingebaut das man als Sensor für
einen Klatschschalter verwenden kann.

<img src="https://pxt.azureedge.net/blob/7d692df59a81d2b7fce4fbb1e4cdb9f536e176e2/static/mb/projects/clap-lights.png" alt="Clap Lights Tutorial" width="200px" align="center">

Besprich zunächst mit deiner Partnerin, was in dem Programm grundsätzlich passieren muss
und fertig eine Beschreibung an.
Probiert es entweder direkt selber aus oder setzt das [Tutorial auf der Make Code Seite](https://makecode.microbit.org/) (siehe Tutorials) um.


**Compass (Kompass)**

> Wie baut man einen Kompass mit dem Micro Bit?

Ein weiterer Sensor den wir besprochen haben ist der Kompass.
In dieser Übung lesen wir den Wert des Kompasssensors aus und zeigen (grob) an,
in welche Richtung der Micro Bit schaut.

<img src="https://pxt.azureedge.net/blob/d3e34ce80d0f558d6dda199a5c33f9770ab93d2f/static/mb/projects/a5-compass.png" alt="Clap Lights Tutorial" width="200px" align="center">

Such dir das [Tutorial auf der Make Code Seite](https://makecode.microbit.org/) (siehe Tutorials)
heraus und setze es um.


**Micro Chat**

> Können die Micro Bits miteinander kommunizieren?

Eine der Schnittstellen des Micro Bits, die wir in der Klasse diskutiert haben, ist die BLE Antenna.
Einzelne Micro Bits können damit miteinander kommunizieren und man kann Informationen austauschen.

<img src="https://pxt.azureedge.net/blob/d8cf0f404e04aa67a9f8ed773b2386aa202776de/static/mb/projects/a9-radio.png" alt="Clap Lights Tutorial" width="200px" align="center">

Besprich zunächst mit deiner Partnerin, was in dem Programm grundsätzlich passieren muss
und fertige eine Beschreibung an.
Such dir dann das [Tutorial auf der Make Code Seite](https://makecode.microbit.org/) (siehe Tutorials)
heraus und setze es um.
```


```{exercise} Wie geht es weiter ...?
:label: exercise-job-self-assessment

Am Mittwoch habt ihr euch ausführlicher zu Berufen und Ausbildungen unterhalten.
Welche Berufe oder Ausbildungen könntest du dir für dich vorstellen?

Trage deine Gedanken dazu [hier](https://docs.google.com/document/d/1tm0BvkZtS-oFaGNKqKZmDmYGNcyq4N16Fcz4SWbXYk8/edit?usp=sharing).


Zu Abschluss haben wir hier noch zwei kurze Videos zu Berufen, die wir in der Form
noch weniger besprochen haben:
- [Informations- & Telekommunikationstechnikerin](https://www.whatchado.com/de/stories/esther-binder)
- [IT-Systemkauffrau](https://www.whatchado.com/de/stories/vivien-deutsche-telekom)

Was sagst du dazu?
```
