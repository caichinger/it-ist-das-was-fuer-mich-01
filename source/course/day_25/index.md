# Tag 25: Abschluss

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`

Heute ist der letzte Kurstag.

Wir wollen heute Kurs mit euren Projektpräsentationen zum
Abschluss bringen und nächste Schritte besprechen.

Wir bitten euch auch um [Feedback](https://forms.gle/pj9w93rrXwoY2JnGA),
damit wir verstehen, wie euch der Kurs gefallen hat und was wir
in Zukunft noch besser machen können.

Wir freuen uns sehr, dass du dabei warst und wünschen dir für
den weiteren Berufsweg alles, alles Gute.
