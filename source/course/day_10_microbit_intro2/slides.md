---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (claus.aichinger@gmail.com)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 10

Programmieren mit dem BBC micro:bit!

---

## Check In

### Wie geht's?

*Was ist dir für heute wichtig?*

---

## Administratives I

Bitte PCR-Testnachweise.

---

## Administratives II

Olga:
> **Arbeitsunfähigkeitsmeldungen bis spätestens heute bringen.**
> Heute geht Eintrag an das AMS.
> Ohne Arbeitsunfähigkeitsmeldung ist es unentschuldigtes Fehlen.

---

## Administratives III

Bitte auf Sauberkeit achten.

Bitte kein Essen herumstehen lassen.

---

## Intro 🌅

*Big Picture*

---

## Was passiert heute? 🗒️

- Wiederholung
- Diskussion
- Programmieren

---

## Was haben wir gestern gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Wiederholung und Diskussion: Allgemein

- Wie arbeite ich in einem (geteilten) Dokument?
- Was ist der File Browser/Explorer (Dateimanager)?
- Wie kopiere ich eine Datei?
- Wie lege ich Favoriten/Bookmarks im Browser an?

---

## Wiederholung und Diskussion: Micro Bit

- Was ist gleich/anders wenn ich Chrome installiere verglichen mit einem Programm für den Micro Bit?
- Welche Art von Blöcke kennt ihr?
- Wenn ich ein Programm für den Micro Bit erstelle, was muss grundsätzlich passieren?

---

## Weiter auf unserer Website

**https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/**

---

## Was haben wir heute gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Outro 🌆

*Tagesabschluss*

---

### Was war heute?

- Wiederholung
- Allgemeine IT (Office) Basics
- Programmieren mit dem Micro Bit

---

### Was kommt morgen

- Soft Skills mit Daniela
- Schwerpunkt auf Berufsbilder

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Ich freue mich auf morgen! 😃
