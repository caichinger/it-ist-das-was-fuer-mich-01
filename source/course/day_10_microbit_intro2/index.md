# Tag 10: BBC micro:bit Intro II

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`


Wir machen heute folgende Übungen:
1. {ref}`Wiederholung Würfel <exercise-micro-bit-dice>`
1. {ref}`exercise-micro-bit-emoji-dice`
1. {ref}`exercise-micro-bit-e-textile-1`
