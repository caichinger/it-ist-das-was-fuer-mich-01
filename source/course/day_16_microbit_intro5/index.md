# Tag 16: BBC micro:bit Intro V

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`

Auch heute haben wir wieder ein paar Übungen geplant.

## Übungen

Wir werden Schere, Stein, Papier fertig machen.
Dazu fehlt uns noch, dass wir zwei Micro Bits miteinander kommunizieren lassen
und die Regeln programmieren.
- {ref}`exercise-basic-radio`
- {ref}`exercise-rock-paper-scissor-part-2`

Außerdem werden wir uns das FiT Programm des AMS ansehen und
einen Blick auf die Erweiterungen für den Micro Bit werfen.


## AMS: FiT - Frauen in Handwerk und Technik

>  Sie möchten einen Job mit Perspektive, in dem Sie gut verdienen? Haben Sie schon an eine Ausbildung im Bereich „Handwerk und Technik“ gedacht? Denn gut ausgebildete Fachkräfte sind am Arbeitsmarkt sehr gefragt – und gut bezahlt.

Es werden **mehr als 200 verschiedene Ausbildungen in Handwerk und Technik gefördert**.

Das FiT-Programm hat eine eigene [Webseite](https://www.ams.at/arbeitsuchende/aus-und-weiterbildung/fit-frauen-in-handwerk-und-technik).

Auf dieser sind auch alle der mehr als 200 geförderten Ausbildungen gelistet.

Insbesondere werden auch kürzere Ausbildungen aus den Bereichen **"Digitale Jobs & Green Jobs"** gefördert.

```{exercise} Wie geht es weiter ...?
:label: exercise-job-self-assessment-2

Recherchiere mit Hilfe der unten gelisteten Werkzeuge Berufe und Ausbildungen die dich ansprechen.

* [Liste aller Fit Ausbildungen](https://www.ams.at/content/dam/download/allgemeine-informationen/001_fit_ausbildungsliste.pdf)
* [Karrierekompass](https://www.karrierekompass.at/)
* [Ausbildungskompass](https://www.ausbildungskompass.at/)

Du kannst auch auf Webseiten von Schulungsanbietern Angebote heraussuchen,
die dich ansprechen.

Beispiele:
* [WAFF](https://www.waff.at/jobs-ausbildung/jobs-mit-ausbildung/it-jobs-informationstechnologie/)
* [BFI](https://www.bfi.wien/)
* ...

Trage deine Gedanken dazu [hier](https://docs.google.com/document/d/1tm0BvkZtS-oFaGNKqKZmDmYGNcyq4N16Fcz4SWbXYk8/edit?usp=sharing) ein.
```

