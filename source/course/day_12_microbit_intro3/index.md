# Tag 12: BBC micro:bit Intro III

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`


Wir machen heute folgende Übung(en):
- {ref}`exercise-micro-bit-retrospective`

und arbeiten an euren Projekten weiter:
- {ref}`exercise-project-step-counter`
- {ref}`exercise-project-emoji-mask`
- {ref}`exercise-project-wristband`
