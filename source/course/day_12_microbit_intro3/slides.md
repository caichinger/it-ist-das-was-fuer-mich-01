---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (claus.aichinger@gmail.com)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 12

Programmieren mit dem BBC micro:bit!

> Kleine Schritte, Stück für Stück

---

## Check In

### Wie war es gestern?

*Was habt ihr gemacht? Was war interessant?*

---

## Intro 🌅

*Big Picture*

---

## Was passiert heute? 🗒️

- Wiederholung & Diskussion
- Programmieren
- Eure Projekte

---

## Was haben wir bisher gelernt? 📝

### Wichtige Ideen :building_construction:

Wir bauen Systeme Stück für Stück aus Einzelteilen (Modulen) zusammen.

> Programmieren = Mit einer Programmiersprache gewünschtes Verhalten beschreiben

> IT = Systeme verstehen

> IT = Systeme aus Einzelteilen zusammen setzen

---

### Was sind **Einzelteile** und wie passen sie zusammen?

Programm:
- Loops
- Logic
- Variables
- I/O
  - LED
  - Buttons
  - ...
- ...

Hardware:
- Komponenten (siehe Beispiele)

---

### Was sind Einzelteile und **wie passen sie zusammen**?

> Schnittstellen!

---

### Wichtige Idee

Wenn ich nicht weiß, wie etwas funktioniert:

> Gibt es etwas einfacheres, das ähnlich ist, von dem ich aber weiß, wie es funktioniert?

---

## Weiter auf unserer Website

**https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/**

---

## Was haben wir heute gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Outro 🌆

*Tagesabschluss*

---

### Was war heute?

- Wiederholung
- Programmieren mit dem Micro Bit
- Projektarbeit

---

### Was kommt morgen

Peer Learning
- Noch ein Berufsbild
- Zusätzliche Micro Bit Features
- Eure Projekte

Montag kommt eine Spieleentwicklerin zu uns! 🕹️

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Ich freue mich auf morgen! 😃
