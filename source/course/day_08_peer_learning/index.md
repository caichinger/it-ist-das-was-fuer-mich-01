# Tag 8: Peer Learning

Heute steht Peer Learning am Programm.


```{include} ../_peer_learning.md
```

Dokumentiere deinen heutigen Fortschritt in [diesem Dokument](https://docs.google.com/document/d/1tjXQ1RJE46h4iXacIKJrN_myCONdQTTG4Fp1cvcbat4/edit?usp=sharing).

Solltest ud mit den hier gestellten Aufgaben fertig sein, dann kannst du entweder eine weitere Aufgabe auf dem Spielfeld lösen oder bei den Übungen weitermachen.


```{exercise} Das Spielfeld - Eine Aufgabe lösen
:label: exercise-solve-a-exercise

Am Dienstag haben wir gemeinsam versucht eine Aufgabe auf dem Spielfeld für den EV3 zu lösen. Du hast heute die Möglichkeit dies selber auszuprobieren. Deine ToDo's sind:
- Beschreibe die Aufgabe, die du machen möchtest
- Überlege dir, was der Roboter braucht um die Aufgabe zu lösen
- Schreibe einen Algorithmus/Lösungsweg für den Roboter
- Programmiere die Aufgabe
- Schau nach ob der Algorithmus gepasst hat? Was musstest du verändern?

```


```{exercise} Das Spielfeld - Reflexion
Diese Aufgabe ist für den Abschluss des Tages.
Beantworte die folgenden Fragen:
- Wie hat es dir heute gefallen die Aufgabe auf dem Spielfeld zu lösen?
- Wie hast du dich dabei gefühlt die Aufgabe zu lösen?
- Könntest du dir vorstellen das jeden Tag zu machen?
```

```{exercise} Was gefällt dir an der IT?
:label: exercise-motivation-it-2

Du hattest in den letzten tagen zeit etwas zu programmieren. Du hast auch schon einige Konzepte der Programmierung kennengelernt. Außerdem konntest du den Roboter umbauen und seine Motoren/Sensoren verwenden.

Stelle dir vor, du erzählst einer Freundin, dass du den Berufsorientierungskurs "IT, ist das
was für mich?" besuchst.

Wie würdest du diese Frage "IT, ist das was für mich?" im Moment beantworten und warum?

Denke nach und schreibe einen kleinen Aufsatz (ca. 400 Zeichen).
```


