# Tag 14: BBC micro:bit Intro IV

```{toctree}
:hidden:
:glob:

*
```

{download}`🎬 Slides <./slides.pdf>`

Wir machen heute folgende Übungen:

- Am Mittwoch habt ihr euch ausführlicher zu Berufen und Ausbildungen
  unterhalten. Welche Berufe oder Ausbildungen könntest du dir für dich vorstellen?
  Trage deine Gedanken dazu [hier](https://docs.google.com/document/d/1tm0BvkZtS-oFaGNKqKZmDmYGNcyq4N16Fcz4SWbXYk8/edit?usp=sharing)
  (das ist der erste Teil der Peer Learning Übung {ref}`exercise-job-self-assessment`).
- {ref}`exercise-questions-for-sophie`
- {ref}`exercise-rock-paper-scissor-part-1`


```{exercise} Unsere Fragen an Sophie (Game Dev)
:label: exercise-questions-for-sophie

Sophie Amelien ist Spieleentwicklerin.
Sie arbeitet als Programmiererin bei [Mi’pu’mi Games](https://www.mipumi.com/).

Sie wird uns heute besuchen.

Fragen:
* Was macht die Firma?
* Welche Fragen, möchtest du Sophie stellen?
```
