# Tag 5: Peer Learning

Gratulation zur ersten Kurswoche! 🎊

Heute steht Peer Learning am Programm.


```{include} ../_peer_learning.md
```

Wir haben uns einige Übungen überlegt.

Die erste Übung ist Pflicht, aus den anderen wähle noch 3 weitere aus.

Wenn du mit den Übungen fertig bist, kannst du weiter mit dem Roboter spielen.


```{exercise} Was gefällt dir an der IT?
:label: exercise-motivation-it-1

Wir haben diese Woche schon einiges kennen gelernt und ausprobiert.

Stelle dir vor, du erzählst einer Freundin, dass du den Berufsorientierungskurs "IT, ist das
was für mich?" besuchst.

Wie würdest du diese Frage "IT, ist das was für mich?" im Moment beantworten und warum?

Denke nach und schreibe einen kleinen Aufsatz (ca. 400 Zeichen) in diesem [Dokument](https://docs.google.com/document/d/15vM0A8nbu5tkeLTPs7B79rAR2onGC2KsQexg9-qJW8E/).
```

```{exercise} Anforderungen und Missverständnisse
:label: exercise-requirements

Was möchte uns dieses Bild sagen?

![Requirements meme](https://miro.medium.com/max/996/1*nhVE3RkKyhk2kEh3nuk3Lw.png)

Fragen:
- Hat du im Alltag schon Situationen erlebt, wo du missverstanden wurdest oder du eine andere Person
falsch verstanden hast?
- Warum war das so?
- Was kann man tun, um solche Missverständnisse zu vermeiden?

Diskutiert in Kleingruppen und schreibt eine Checkliste mit Dingne die wichtig sind, wenn
man Anforderungen formuliert.

Die Checkliste kann entweder auf einem Blatt Papier sein, das ihr der Betreuungsperson gebt
oder ihr fügt sie [hier](https://docs.google.com/document/d/15vM0A8nbu5tkeLTPs7B79rAR2onGC2KsQexg9-qJW8E/) hinzu.
```

```{exercise} Beschreibe 5 Begriffe von unserer Begriff-Sammel-Wand
:label: exercise-describe-terms-1

Diese Woche hast du viele neue Begriffe gelernt. Diese Begriffe werden dich später auf deinem Weg sehr wahrscheinlich begleiten.

Dein ToDo:
- Suche dir 5 Begriffe (oder mehr, falls du Lust hast) und schreibe eine Erklärung dazu
- Lies deine Erklärung (müssen nicht alle sein) einer anderen Person vor
- Frage die Person, ob sie die Erklärung verstanden hat
```

```{exercise} Schreibe eine Anleitung
:label: exercise-design-an-algorithm

In dieser Aufgabe werden alle Dinge, die du gelern hast zu einem vereint und ist schon fast ein kleines Projekt. Du kannst diese Aufgabe gerne in Partnerarbeit erledigen, da Projekte zu zweit viel mehr Spaß machen.
Projektbeschreibung:
- Überlege dir eine Aufgabe, die der Roboter erledigen soll (z.b. um eine schwarze Kiste fahren, einer linie folgen, ...)
- Beschreibe die Aufgabe
- Schreibe einen Algorithmus auf Papier
- Nimm den Algorithmus und programmier ihn für den Roboter

Macht der Roboter was du willst?

```

```{exercise} Was macht eine IT-Systemtechnikerin
:label: exercise-design-a-manual

In Österreich wurden vor kurzem die IT-Lehrberufe neu gestaltet und es sind drei Lehrberufe entstanden:
- Applikationsentwicklung Coding
- Informationstechnologie - Schwerpunkt Systemtechnik
- Informationstechnologie - Schwerpunkt Betriebstechnik

Du kannst dich zu allen Lehrberufen auf [it-lehre.wien](https://it-lehre.wien/) informieren.

Bei dieser Aufgabe schaust du dir zu einem Lehrberuf [in diesem Video](https://www.whatchado.com/de/stories/jasmin-gastgeb) an.

Beantworte folgende Fragen:
- Was macht Jasmin Gastgeb?
- Was macht sie in ihrer Lehre?
- Schreibe 3 Begriffe auf, die du nicht verstanden hast.
```
