# Tag 15: Kommunikation und Präsentation

## Christine (Beispielpräsentation)
* [Präsentation](https://docs.google.com/presentation/d/1c-ibHQFYlLgC-0ftVULa__f9YWbseGHhgX8s23TtHE0/edit?usp=sharing)

## Lejla
* [Präsentation](https://docs.google.com/presentation/d/1Up-tFGq1nsDTQZK2wk7LTHZDurNUxOSgVNnX9xov7rk/edit?usp=sharing)

## Manuela
* [Präsentation](https://docs.google.com/presentation/d/1l5XsE2TJWj0gW7TycjCIqjaqJady65ihJq3t-ob9GYE/edit?usp=sharing)

## Nurten
* [Präsentation](https://docs.google.com/presentation/d/1Ii7fjSpJVAzNwi8HI8JGSTZYHt2r4etRKjuWVrckll4/edit?usp=sharing)

## Sanaa
* [Präsentation](https://docs.google.com/presentation/d/1_ahsKQ4LfmYKFwfMzzvhYDArk7AFR9cf0ZOg6HHNHZ8/edit?usp=sharing)

## Sara
* [Präsentation](https://docs.google.com/presentation/d/1DN3bsQQWxp_x_FIM707UpVxQM0AeALBSe1EFKZqJ_ak/edit?usp=sharing)

## Sham
* [Präsentation](https://docs.google.com/presentation/d/1V9KBTusTIsvLw0LbQBizWIcMzCosAb7CftyBOQA3pNc/edit?usp=sharing)

## Simran
* [Präsentation](https://docs.google.com/presentation/d/1dAv69jIeuyImVkeB3vIueMy3p6PInmZJwqrEMr8oXRQ/edit?usp=sharing)

## Viyan
* [Präsentation](https://docs.google.com/presentation/d/1iDKc9pcnTNK67J5H-KzmtcCFZo_Wlw4hihwWeHW5ibo/edit?usp=sharing)
