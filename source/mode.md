---
marp: true
theme: default
---


# Zusammenarbeit

## Was uns im Kurs wichtig ist

...

## Hausregeln

...

## Allfälliges

- 🚪 Eingangstüre: bitte darauf achten, dass
  diese immer geschlossen ist (sie bleibt manchmal stecken)
- 😷 Maskenpflicht: bitte immer FFP2 Maske tragen
- 🤒 Krankmeldungen und Zeitbestätigungen: bitte ab jetzt
  (wenn möglich) per Mail